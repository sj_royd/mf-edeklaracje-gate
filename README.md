# eDeklaracje gate

A polish Ministry of Finances VAT declarations WebService gate.

## Usage

### sendDocument service

The **sendDocument()** service is used to send electronic documents. It is required that each submitted document be signed (XAdES or PKCS # 7) using the appropriate certificate, and have the appropriate structure.

Input data:

- **$document** - a binary form of a signed XML document encoded in accordance with the Base64Binary format. The document should be in a format consistent with the current schema.

Response methods:

- **getRefId()** - Reference Number - string type - number of the submitted document. The reference number is not returned for a message for a document that cannot be processed.
- **getStatus()** - Status - type int - the status number that the submitted document has received.
- **getStatusDesc()** - Status description - string type - description of the status of the submitted document.
- **getMessage()** - a string of _(getStatus()) getStatusDesc()_ ex. _(212) A message_

Possible document statuses:

- 100-199 A group of messages for documents that cannot be processed (faulty SOAP message) or a critical server error occurred.
- 300-399 A group of messages for documents being processed.

The service currently returns the following statuses:

- 100 - Incorrect SOAP message
- 101 - Please send me the document again
- 300 - No document
- 301 - Document being processed, check the result of the next document verification
- 302 - Pre-processed document, check the result of the next document verification
- 303 - Document is being verified, check the result of the next document verification
- 306 - Document is being verified, please check the result of the next document verification

```php
<?php

use SJRoyd\MF\EDeklaracje\Dokumenty;

$doc = '<?xml>';
// or
$doc = fopen('file.xml', 'r');

$d = new Dokumenty();
$response = $d->sendDocument($doc);

$response->getRefId();
$response->getStatus();
$response->getStatusDesc();
```

### sendUnsignDocument service

The **sendUnsignDocument()** service is used to send electronic documents. It is required that each submitted document contains the <DataAutoryzujace> element or <DaneAutoryzujaceVAP> element, added in the <Declaration> element as the last element, and has the appropriate structure.

Input data:

- **$document** - a binary form of XML document encoded in accordance with the Base64Binary format. The document should be in a format consistent with the current schema.
- **$lang** - Language version in some document data (optional "language" element). It can be "de", "en" or "pl". Lack of this element means choosing Polish.
- **$signType** - Type of electronic signature used (optional "signatureType" element). It can take the value "PIT" for a document with the "authorization data" signature or "MPO" for a document with the "VAP authorization data" signature. The absence of this element means the use of a "authorization data" signature.

Response methods are the same like above.

Possible document statuses:

- 100-199 A group of messages for documents that cannot be processed (faulty SOAP message) or a critical server error occurred.
- 300-399 A group of messages for documents being processed.

The service currently returns the following statuses:

- 100 - Incorrect SOAP message
- 101 - Please send me the document again
- 300 - No document
- 301 - Document being processed, check the result of the next document verification
- 302 - Pre-processed document, check the result of the next document verification

```php
<?php

use SJRoyd\MF\EDeklaracje\Dokumenty;

$doc = '<?xml>';
// or
$doc = fopen('file.xml', 'r');

$d = new Dokumenty();
$response = $d->sendUnsignDocument($doc, 'pl', 'PIT');
```

### sendDocumentWithAttachment service

The **sendDocumentWithAttachment()** service is used to send electronic documents with a binary attachment. It is required that each submitted document be signed (XAdES or CAdES [PKCS # 7]) using the appropriate certificate and have the appropriate structure.

Input data:

- **$document** - a signed XML document. The document should be in a format consistent with the current schema.
- **$attachment** - a ZIP document encoded (optional).

Response methods are the same like above.

Possible document statuses:

- 100-199 A group of messages for documents that cannot be processed (faulty SOAP message) or a critical server error occurred.
- 300-399 A group of messages for documents being processed.

The service currently returns the following statuses:

- 100 - Incorrect SOAP message
  - Attention! Depending on the type of error detected in the attachment, the message can be extended with the text: "the attachment exceeds the allowable length" or "incorrect ZIP file format".
- 101 - Please send me the document again
- 300 - No document
- 301 - Document being processed, check the result of the next document verification
- 302 - Pre-processed document, check the result of the next document verification 
- 303 - Document is being verified, check the result of the next document verification
- 306 - Document is being verified, please check the result of the next document verification

```php
<?php

use SJRoyd\MF\EDeklaracje\Dokumenty;

$doc = '<?xml>';
// or
$doc = fopen('file.xml', 'r');

$attachment = file_get_contents('file.zip');
// or
$attachment = fopen('file.xml', 'r');

$d = new Dokumenty();
$response = $d->sendDocumentWithAttachment($doc, $attachment);
```

### RequestUPO service

The requestUPO service is used to download the Official Certificate of Receipt (UPO) for the given Document Reference Number.

Input data:

- **$refId** - Reference number of the submitted, sendDocument or sendUnsignDocument or sendDocumentWithAttachment operation, document,
- **lang** - The language version in which the electronic document processing status description should be returned (optional element "language"). It can be "de", "en" or "pl". Lack of this element means choosing Polish.

Response methods:

- **hasUpo()** - type boolean - an information there is the UPO
- **getUpo()** - UPO - a signed XML document of the type string, returned only if it was generated.
- **getStatus()** - Status - type int - status number received by the submitted document.
- **getStatusDesc()** - Status description - string type - description of the status of the submitted document.

Possible document statuses:

- 100-199 Group of messages for documents that cannot be processed (faulty SOAP message) or a critical server error occurred.
- 200-299 A group of messages for documents that are correct or for additional interaction.
- 300-399 A group of messages for documents being processed.
- 400-499 Group of messages for incorrect documents due to structural errors of syntax; subsequent statuses indicate the reason for rejecting the document.

The service currently returns the following statuses:

- 100 - Incorrect SOAP message
- 102 - Please resend the UPO request
- 200 - Document processing completed successfully, download UPO
- 200 - You have sent a duplicate, the downloaded UPO is for the original document. Document processing completed successfully, download UPO
- 300 - No document
- 301 - Document being processed, check the result of the next document verification
- 302 - Pre-processed document, check the result of the next document verification
- 303 - Document during signature verification, check the result of the next document verification
- 306 - Document during signature verification, check the result of the next document verification
- 400 - Document processing terminated with error
- 401 - Negative verification - the document does not comply with the xsd schema
- 402 - No current power of attorney / authority to sign the declaration
- 403 - Document with incorrect signature

```php
<?php

use SJRoyd\MF\EDeklaracje\Dokumenty;

$refId = '...'; // An refId from response method getRefId()
$response = $d->requestUPO($refId);

if($response->hasUpo()){
    $upo = $response->getUpo();
}
```