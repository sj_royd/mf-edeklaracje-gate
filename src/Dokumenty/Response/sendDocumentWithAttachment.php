<?php

namespace SJRoyd\MF\EDeklaracje\Dokumenty\Response;

class sendDocumentWithAttachment extends sendDocument
{}


/*
 * 100 - Błędny komunikat SOAP
 * Uwaga! W zależności od wykrytego rodzaju błędu w załączniku komunikat może być
 * poszerzony o tekst: „załącznik przekracza dopuszczalną długość” lub „błędny format pliku ZIP”.
 * 101 – Proszę o ponowne przesłanie dokumentu
 * 300 – Brak dokumentu
 * 301 – Dokument w trakcie przetwarzania, sprawdź wynik następnej weryfikacji dokumentu
 * 302 – Dokument wstępnie przetworzony, sprawdź wynik następnej weryfikacji dokumentu
 * 303 – Dokument w trakcie weryfikacji podpisu, sprawdź wynik następnej weryfikacji dokumentu
 * 306 - Dokument w trakcie weryfikacji podpisu, sprawdź wynik następnej weryfikacji dokumentu
 */