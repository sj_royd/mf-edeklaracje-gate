<?php

namespace SJRoyd\MF\EDeklaracje\Dokumenty\Response;

class sendDocument {

    /**
     * @var string|null
     */
    private $refId;
    /**
     * @var int
     */
    private $status;
    /**
     * @var string
     */
    private $statusOpis;

    /**
     * @param   string|null  $refId
     *
     * @return sendDocument
     */
    public function setRefId($refId)
    {
        $this->refId = $refId;

        return $this;
    }

    /**
     * @param   int  $status
     *
     * @return sendDocument
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param   string  $statusOpis
     *
     * @return sendDocument
     */
    public function setStatusOpis($statusOpis)
    {
        $this->statusOpis = $statusOpis;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRefId()
    {
        return $this->refId;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getStatusDesc()
    {
        return $this->statusOpis;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return "($this->status) {$this->statusOpis}";
    }

}

/*
 * 100 - Błędny komunikat SOAP
 * 101 – Proszę o ponowne przesłanie dokumentu
 * 300 – Brak dokumentu
 * 301 – Dokument w trakcie przetwarzania, sprawdź wynik następnej weryfikacji dokumentu
 * 302 – Dokument wstępnie przetworzony, sprawdź wynik następnej weryfikacji dokumentu
 * 303 – Dokument w trakcie weryfikacji podpisu, sprawdź wynik następnej weryfikacji dokumentu
 * 306 - Dokument w trakcie weryfikacji podpisu, sprawdź wynik następnej weryfikacji dokumentu
 */