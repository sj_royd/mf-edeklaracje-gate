<?php

namespace SJRoyd\MF\EDeklaracje\Dokumenty\Response;

class sendUnsignDocument extends sendDocument
{}

/*
 * 100 - Błędny komunikat SOAP
 * 101 – Proszę o ponowne przesłanie dokumentu
 * 300 – Brak dokumentu
 * 301 – Dokument w trakcie przetwarzania, sprawdź wynik następnej weryfikacji dokumentu
 * 302 – Dokument wstępnie przetworzony, sprawdź wynik następnej weryfikacji dokumentu
 */