<?php

namespace SJRoyd\MF\EDeklaracje\Dokumenty\Response;

class requestUPO
{

    /**
     * @var int
     */
    private $status;
    /**
     * @var string
     */
    private $statusOpis;
    /**
     * @var string
     */
    private $upo;

    /**
     * @param   int  $status
     *
     * @return requestUPO
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param   string  $statusOpis
     *
     * @return requestUPO
     */
    public function setStatusOpis($statusOpis)
    {
        $this->statusOpis = $statusOpis;

        return $this;
    }

    /**
     * @param   string  $upo
     *
     * @return requestUPO
     */
    public function setUpo($upo)
    {
        $this->upo = $upo;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getStatusDesc()
    {
        return $this->statusOpis;
    }

    /**
     * @return string
     */
    public function getUpo()
    {
        return $this->upo;
    }

    /**
     * @return bool
     */
    public function hasUpo()
    {
        return (bool)$this->upo;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return "($this->status) {$this->statusOpis}";
    }

}

/*
 * 100 - Błędny komunikat SOAP
 * 102 – Proszę o ponowne przesłanie żądania UPO
 * 200 - Przetwarzanie dokumentu zakończone poprawnie, pobierz UPO
 * 200 - Przesłałeś duplikat, pobrane UPO dotyczy oryginału dokumentu.
 *       Przetwarzanie dokumentu zakończone poprawnie, pobierz UPO
 * 300 - Brak dokumentu
 * 301 - Dokument w trakcie przetwarzania, sprawdź wynik następnej weryfikacji dokumentu
 * 302 – Dokument wstępnie przetworzony, sprawdź wynik następnej weryfikacji dokumentu
 * 303 – Dokument w trakcie weryfikacji podpisu, sprawdź wynik następnej weryfikacji dokumentu
 * 306 – Dokument w trakcie weryfikacji podpisu, sprawdź wynik następnej weryfikacji dokumentu
 * 400 - Przetwarzanie dokumentu zakończone błędem
 * 401 – Weryfikacja negatywna - dokument niezgodny ze schematem xsd
 * 402 - Brak aktualnego pełnomocnictwa/upoważnienia do podpisywania deklaracji
 * 403 - Dokument z niepoprawnym podpisem
 * 404 - Dokument z nieważnym certyfikatem
 * 405 - Dokument z odwołanym certyfikatem
 * 406 - Dokument z certyfikatem z nieobsługiwanym dostawcą
 * 407 - Dokument z certyfikatem z nieprawidłową ścieżką
 * 408 - Dokument zawiera błędy uniemożliwiające jego przetworzenie
 * 409 – Dokument zawiera niewłaściwą ilość i/lub rodzaj elementów
 * 410 – Złożony dokument bez podpisu nie może być korektą
 * 411 – Weryfikacja negatywna - w systemie jest już złożony dokument z takim identyfikatorem podatkowym
 * 412 – Weryfikacja negatywna - niezgodność danych autoryzujących z danymi w dokumencie
 *       (np. niezgodność NIP, numeru PESEL, daty urodzenia, nazwiska, pierwszego imienia)
 * 413 – Dokument z certyfikatem bez wymaganych atrybutów
 * 414 – Weryfikacja negatywna - błąd w danych autoryzujących (np. błąd w nazwisku,
 *       pierwszym imieniu, dacie urodzenia, NIP, numerze PESEL, kwocie przychodu)
 * 415 – Zawartość załącznika niezgodna z deklarowaną listą plików
 * 416 – Dla tego typu deklaracji załącznik binarny nie jest dozwolony
 * 417 – Wniosek VAT-REF wymaga przynajmniej jednej z pozycji:
 *       VATRefundApplication lub ProRataRateAdjustment
 * 418 – Dla złożonej deklaracji wymagane jest użycie podpisu kwalifikowanego
 * 419 – Brak zaznaczenia celu złożenia formularza jako korekty deklaracji (zeznania)
 *       lub brak uzasadnienia przyczyny złożenia korekty deklaracji (zeznania)
 * 420 - Użycie podpisu DaneAutoryzujaceVAP jest dozwolone jedynie dla dokumentu VAP-1
 * 421 - Dokument VAP-1 można złożyć jedynie z użyciem podpisu DaneAutoryzujaceVAP
 * 422 – Weryfikacja negatywna - dokument złożony z użyciem danych autoryzujących może
 *       złożyć wyłącznie podatnik, będący osobą fizyczną
 * 423 - Dokument może złożyć wyłącznie podmiot będący osobą fizyczną, niebędący pełnomocnikiem
 * 424 - Nie podano numeru VAT dostawcy lub numeru faktury, a nie jest to faktura uproszczona
 * 425 - Kod państwa członkowskiego identyfikacji dostawcy jest niezgodny z kodem państwa,
 *       do którego kierowany jest wniosek
 * 460 - Nieprawidłowa struktura NIP
 * 461 - Nieprawidłowa struktura Numeru identyfikacyjnego VAT lub go brak
 * 462 - Nieprawidłowa struktura Numeru IBAN
 * 463 - Nieprawidłowa struktura Kodu BIC
 * 464 - W przypadku wyrejestrowania należy wybrać Cel złożenia równy 2
 * 465 - W przypadku zmiany państwa członkowskiego identyfikacji konieczne jest podanie jej szczegółów
 * 466 - Nieprawidłowe podsumowanie kwot
 * 467 - Kod państwa członkowskiego konsumpcji nie może być taki jak kod państwa stałego
 *       miejsca prowadzenia działalności gospodarczej
 */