<?php

namespace SJRoyd\MF\EDeklaracje\Dokumenty\Request;

class sendUnsignDocument extends sendDocument
{
    public $language;

    public $signatureType;

    public function __construct($document, $lang = null, $signType = null)
    {
        parent::__construct($document);
        $this->language = $lang;
        $this->signatureType = $signType;
    }
}
