<?php

namespace SJRoyd\MF\EDeklaracje\Dokumenty\Request;

class sendDocumentWithAttachment extends sendDocument
{
    public $attachment;

    /**
     * @param   string|resource  $document
     * @param   string|resource  $attachment
     * @throws \Exception
     */
    public function __construct($document, $attachment)
    {
        parent::__construct($document);
        $this->prepareDocument($attachment);
        $this->attachment = base64_encode($attachment);
    }
}