<?php

namespace SJRoyd\MF\EDeklaracje\Dokumenty\Request;

class sendDocument
{
    public $document;

    /**
     * @param   string|resource  $document
     * @throws \Exception
     */
    public function __construct($document)
    {
        $this->prepareDocument($document);
        $this->document = base64_encode($document);
    }

    /**
     * @param $src
     * @throws \Exception
     */
    protected function prepareDocument(&$src)
    {
        switch(gettype($src)){
            case 'resource':
                $src = stream_get_contents($src);
                break;
            case 'string':
                break;
            default:
                throw new \Exception('Unexpected type of document source');
        }
    }

}
