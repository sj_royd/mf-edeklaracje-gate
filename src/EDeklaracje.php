<?php

namespace SJRoyd\MF\EDeklaracje;

use SJRoyd\HTTPService\SoapRequest;

/**
 * @see https://www.podatki.gov.pl/media/3615/edek_specyfikacja_we-wy_2-4_www.pdf
 */
class EDeklaracje extends SoapRequest
{

	protected $ws_path   = 'https://bramka.e-deklaracje.mf.gov.pl/uslugi';
	protected $test_path = 'https://test-bramka.edeklaracje.gov.pl/uslugi';
    protected $wsdl = false;

	public function __construct($test = false, $debug = false)
    {
		if($test){
            $this->ws_path = $this->test_path;
		}
		parent::__construct($test, $debug);
	}

    /**
     * @param string $msg
     * @param int    $code
     * @throws \Exception
     */
    protected function error($msg, $code = null)
    {
        throw new \Exception($msg, $code);
    }

}
