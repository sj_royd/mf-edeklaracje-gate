<?php

namespace SJRoyd\MF\EDeklaracje;

use SJRoyd\MF\EDeklaracje\Dokumenty\Request;
use SJRoyd\MF\EDeklaracje\Dokumenty\Response;

class Dokumenty extends EDeklaracje
{
    protected $ws_name = 'dokumenty';

    private $refId;

    /**
     * The service is used to send electronic documents. It is required that each
     * submitted document be signed (XAdES or PKCS # 7) using the appropriate
     * certificate, and have the appropriate structure.
     *
     * @param string|resource $document Document string or file resource
     * @return Response\sendDocument
     * @throws \Exception
     */
    public final function sendDocument($document)
    {
        /* @var $response Response\sendDocument */
        $response = $this->call(
            __FUNCTION__,
            new Request\sendDocument($document),
            [
                200 => Response\sendDocument::class
            ]
        );
        if($this->responseStatusCode != 200){
            $this->error('An error occurred', $this->responseStatusCode);
        }
        if($response->getStatus() < 301){
            throw new \Exception($response->getMessage(), $response->getStatus());
        }
        $this->refId = $response->getRefId();
        return $response;
    }

    /**
     * The service is used to send electronic documents. It is required that each
     * submitted document contains the <DataAutoryzujace> element or <DaneAutoryzujaceVAP>
     * element, added in the <Declaration> element as the last element,
     * and has the appropriate structure.
     *
     * @param   string|resource  $document Document string or file resource
     * @param   string           $lang
     * @param   string           $signType
     * @return Response\sendUnsignDocument
     * @throws \Exception
     */
    public final function sendUnsignDocument($document, $lang = null, $signType = null)
    {
        /* @var $response Response\sendUnsignDocument */
        $response = $this->call(
            __FUNCTION__,
            new Request\sendUnsignDocument($document, $lang, $signType),
            [
                200 => Response\sendUnsignDocument::class
            ]
        );
        if($this->responseStatusCode != 200){
            $this->error('An error occurred', $this->responseStatusCode);
        }
        if($response->getStatus() < 301){
            throw new \Exception($response->getMessage(), $response->getStatus());
        }
        $this->refId = $response->getRefId();
        return $response;
    }

    /**
     * The service is used to send electronic documents with a binary attachment.
     * It is required that each submitted document be signed (XAdES or CAdES [PKCS # 7])
     * using the appropriate certificate and have the appropriate structure.
     *
     * @param string|resource $document Document string or file resource
     * @param string|resource $attachment Document string or file resource
     * @return Response\sendDocumentWithAttachment
     * @throws \Exception
     */
    public final function sendDocumentWithAttachment($document, $attachment)
    {
        /* @var $response Response\sendDocumentWithAttachment */
        $response = $this->call(
            __FUNCTION__,
            new Request\sendDocumentWithAttachment($document, $attachment),
            [
                200 => Response\sendDocumentWithAttachment::class
            ]
        );
        if($this->responseStatusCode != 200){
            $this->error('An error occurred', $this->responseStatusCode);
        }
        if($response->getStatus() < 301){
            throw new \Exception($response->getMessage(), $response->getStatus());
        }
        $this->refId = $response->getRefId();
        return $response;
    }

    /**
     * Służy do pobrania Urzędowego Poświadczenia Odbioru (UPO)
     * dla podanego Numeru Referencyjnego dokumentu
     *
     * @param string $refId
     * @return Response\requestUPO
     * @throws \Exception
     */
    public final function requestUPO($refId = null)
    {
        !$refId && $refId = $this->refId;
        if(!$refId){
            $this->error('Please enter the reference number');
        }
        $response = $this->call(
            __FUNCTION__,
            new Request\requestUPO($refId),
            [
                200 => Response\requestUPO::class
            ]
        );
        return $response;
    }
}